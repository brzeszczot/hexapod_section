#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define MAX_SECTION_SIZE   100

typedef struct
{
	int x;
	int y;
	int z;
} s_coordinates;

int coords_calc(s_coordinates p1, s_coordinates p2, int bytesPerLine, s_coordinates v[MAX_SECTION_SIZE], int deep)
{
	int x1 = (int)p1.x;
	int y1 = (int)p1.y;
	int x2 = (int)p2.x;
	int y2 = (int)p2.y;

	s_coordinates vtemp;
	int d, dx, dy, ai, bi, xi, yi;
	int x = x1, y = y1;

	// determining the direction of drawing
	if (x1 < x2)
	{
        	xi = bytesPerLine;
        	dx = x2 - x1;
    	}
    	else
    	{
        	xi = -bytesPerLine;
        	dx = x1 - x2;
    	}

    	// determining the direction of drawing
	if (y1 < y2)
    	{
        	yi = bytesPerLine;
        	dy = y2 - y1;
    	}
    	else
    	{
        	yi = -bytesPerLine;
        	dy = y1 - y2;
    	}
    	// first pixel

    	vtemp.x = x;
    	vtemp.y = y;

    	v[0] = vtemp;
    	int ind = 1;

    	// the leading axis OX
    	if (dx > dy)
    	{
        	ai = (dy - dx) * 2;
        	bi = dy * 2;
        	d = bi - dx;

        	// loop for x
        	while (x <= (x2-bytesPerLine)||x > (x2+bytesPerLine))
        	{
			// factor test
            		if (d >= 0)
            		{
                		x += xi;
                		y += yi;
                		d += ai;
            		}
            		else
            		{
                		d += bi;
                		x += xi;
            		}

            		vtemp.x = x;
            		vtemp.y = y;

            		v[ind] = vtemp;
            		ind++;
        	}
    	}
    	// the leading axis OY
    	else
    	{
        	ai = (dx - dy) * 2;
        	bi = dx * 2;
        	d = bi - dy;
        
		// loop for y
        	while (y <= (y2-bytesPerLine)||y > (y2+bytesPerLine))
        	{
            		// factor test
            		if (d >= 0)
            		{
                		x += xi;
                		y += yi;
                		d += ai;
            		}
            		else
            		{
                		d += bi;
                		y += yi;
            		}

            		vtemp.x = x;
            		vtemp.y = y;

            		v[ind] = vtemp;
            		ind++;
        	}
    	}

	return ind;
}

int main(int argc, char** argv)
{
	printf("Section\n\n");

	s_coordinates v[MAX_SECTION_SIZE];
	s_coordinates p1, p2;
	
	p1.x = 2;
	p1.y = 2;
	p2.x = 13;
	p2.y = 6;

	int size;

	size = coords_calc(p1, p2, 1, v, -1);
	printf("Size: %d\n\n", size);

	for(int ii = 0; ii < size; ii++)
		printf("x: %d, y: %d, ", v[ii].x, v[ii].y);

	printf("\n\n");

	return 0;
}
