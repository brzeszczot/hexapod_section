#include <stdio.h>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <vector>

typedef struct
{
	int x;
	int y;
	int z;
} Vector2f;


Vector2f SectionCalc(int x1, int y1, int x2, int y2, int bytesPerLine, std::vector<Vector2f>* v, int deep)
{
    Vector2f vtemp;
    int d, dx, dy, ai, bi, xi, yi;
    int x = x1, y = y1;
    // determining the direction of drawing
    if (x1 < x2)
    {
        xi = bytesPerLine;
        dx = x2 - x1;
    }
    else
    {
        xi = -bytesPerLine;
        dx = x1 - x2;
    }
    // determining the direction of drawing
    if (y1 < y2)
    {
        yi = bytesPerLine;
        dy = y2 - y1;
    }
    else
    {
        yi = -bytesPerLine;
        dy = y1 - y2;
    }
    // first pixel

    vtemp.x = x;
    vtemp.y = y;
    v->push_back(vtemp);
    if((int)v->size()>=deep && deep!=-1) return vtemp;
    //SetPixel(x, y, color);
    // the leading axis OX
    if (dx > dy)
    {
        ai = (dy - dx) * 2;
        bi = dy * 2;
        d = bi - dx;
        // loop for x
        while (x <= (x2-bytesPerLine)||x > (x2+bytesPerLine))
        {
            // factor test
            if (d >= 0)
            {
                x += xi;
                y += yi;
                d += ai;
            }
            else
            {
                d += bi;
                x += xi;
            }

            vtemp.x = x;
            vtemp.y = y;
            v->push_back(vtemp);
 	    //SetPixel(x, y, color);
            if((int)v->size()>=deep && deep!=-1) return vtemp;
        }
    }
    // the leading axis OY
    else
    {
        ai = (dx - dy) * 2;
        bi = dx * 2;
        d = bi - dy;
        // loop for y
        while (y <= (y2-bytesPerLine)||y > (y2+bytesPerLine))
        {
            // factor test
            if (d >= 0)
            {
                x += xi;
                y += yi;
                d += ai;
            }
            else
            {
                d += bi;
                y += yi;
            }

            vtemp.x = x;
            vtemp.y = y;
            v->push_back(vtemp);
            //SetPixel(x, y, color);
            if((int)v->size()>=deep && deep!=-1) return vtemp;
        }
    }
    return vtemp;
}


int main(int argc, char** argv)
{
	printf("Section\n\n");

	std::vector<Vector2f>* v;

	v = new std::vector<Vector2f>;
	SectionCalc(2, 2, 5, 8, 1, v, -1);

	printf("Size: %d\n\n", v->size());
	
	for(int ii = 0; ii < (int)v->size(); ii++)
		printf("x: %d, y: %d, ", (*v)[ii].x, (*v)[ii].y);

	printf("\n\n");

	return 0;
}
